﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Employer.Models;
using System.Data;
using System.Configuration;

namespace Employer.Controllers
{
    public class DepartmentController : ApiController
    {
        // GET api/department/5
        public department Get(int id)
        {
            department objDepartment = new department();
            SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);           
            SqlDataAdapter adap = new SqlDataAdapter("getDepartmentDetails", sqlConn);
            DataTable dtDepartment = new DataTable();
            sqlConn.Open();
            adap.Fill(dtDepartment);
            sqlConn.Close();

            if(dtDepartment.Rows.Count > 0)
            {
                objDepartment.deparment_id = Convert.ToInt32(dtDepartment.Rows[0]["department_id"].ToString());
                objDepartment.deparment_code = dtDepartment.Rows[0]["department_code"].ToString();
                objDepartment.deparment_code = dtDepartment.Rows[0]["department_code"].ToString();
            }
            
            return objDepartment;
        }

        // POST api/department
        public void Post([FromBody]department departmentData)
        {
            SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            SqlCommand sqlCmd = new SqlCommand("saveDepartmentDetails", sqlConn);           
            sqlCmd.Parameters.AddWithValue("deparment_code", departmentData.deparment_code);
            sqlCmd.Parameters.AddWithValue("deparment_name", departmentData.deparment_name);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlConn.Open();
            sqlCmd.ExecuteNonQuery();
            sqlConn.Close();
        }

        // PUT api/department/5
        public void Put(int id, [FromBody]department departmentData)
        {
            SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            SqlCommand sqlCmd = new SqlCommand("updateDepartmentDetails", sqlConn);
            sqlCmd.Parameters.AddWithValue("deparment_id", departmentData.deparment_id);
            sqlCmd.Parameters.AddWithValue("deparment_code", departmentData.deparment_code);
            sqlCmd.Parameters.AddWithValue("deparment_name", departmentData.deparment_name);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlConn.Open();
            sqlCmd.ExecuteNonQuery();
            sqlConn.Close();
        }

        // DELETE api/department/5
        public void Delete(int id)
        {
            SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            SqlCommand sqlCmd = new SqlCommand("deleteDepartmentDetails", sqlConn);
            sqlCmd.Parameters.AddWithValue("deparment_id", id);            
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlConn.Open();
            sqlCmd.ExecuteNonQuery();
            sqlConn.Close();
        }
    }
}
