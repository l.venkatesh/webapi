﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Employer.Models;

namespace Employer.Controllers
{
    public class InternController : ApiController
    {
        // GET api/intern/5
        public intern Get(int id)
        {
            intern objIntern = new intern();
            SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            SqlDataAdapter adap = new SqlDataAdapter("getInternDetails", sqlConn);
            DataTable dtIntern = new DataTable();
            sqlConn.Open();
            adap.Fill(dtIntern);
            sqlConn.Close();

            if (dtIntern.Rows.Count > 0)
            {
                objIntern.intern_id = Convert.ToInt32(dtIntern.Rows[0]["intern_id"].ToString());
                objIntern.intern_name = dtIntern.Rows[0]["intern_name"].ToString();
                objIntern.deparment_id = Convert.ToInt32(dtIntern.Rows[0]["department_id"].ToString());
            }

            return objIntern;
        }

        // POST api/intern
        public void Post([FromBody]intern internData)
        {
            SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            SqlCommand sqlCmd = new SqlCommand("saveInternDetails", sqlConn);
            sqlCmd.Parameters.AddWithValue("intern_name", internData.intern_name);
            sqlCmd.Parameters.AddWithValue("deparment_id", internData.deparment_id);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlConn.Open();
            sqlCmd.ExecuteNonQuery();
            sqlConn.Close();
        }

        // PUT api/intern/5
        public void Put(int id, [FromBody]intern internData)
        {
            SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            SqlCommand sqlCmd = new SqlCommand("updateInternDetails", sqlConn);
            sqlCmd.Parameters.AddWithValue("intern_id", internData.intern_id);
            sqlCmd.Parameters.AddWithValue("intern_name", internData.intern_name);
            sqlCmd.Parameters.AddWithValue("deparment_id", internData.deparment_id);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlConn.Open();
            sqlCmd.ExecuteNonQuery();
            sqlConn.Close();
        }

        // DELETE api/intern/5
        public void Delete(int id)
        {
            SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            SqlCommand sqlCmd = new SqlCommand("deleteInternDetails", sqlConn);
            sqlCmd.Parameters.AddWithValue("intern_id", id);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlConn.Open();
            sqlCmd.ExecuteNonQuery();
            sqlConn.Close();
        }
    }
}
